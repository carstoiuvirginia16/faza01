# faza01

Administrare locuri de parcare cu Google Maps

Aplicatia va permite utilizatorilor sa aiba acces la informatii privind disponibilitatea locurilor de parcare 
aflate in imediata vecinatate a destinatiei alese.
Acest manager al locurilor de parcare va fi integrat cu Google Maps, iar odata cu alegerea unei rute din punctual A 
catre punctul B, pe harta vor fi evidentiate cu ajutorul unor marcaje parcarile din apropierea punctului final. 
Astfel, parcarile cu un numar mare de locuri vor fi reprezentate de simbolul P colorat in verde,
iar spatiile cu disponibilitate redusa vor fi evidentiate de simbolul P colorat in rosu.
Acest lucru va putea fi realizat datorita faptului ca Google se bazeaza pe accesul la parcarile cu plata, 
contorizand numarul de vehicule parcate, iar odata conectate la Google Maps,
aplicatia va putea transmite numarul de locuri libere si locatia acestora.
De asemenea, aceasta functie va fi disponibila din momentul in care soferii conduc spre destinatie, 
nu in momentul in care ajung acolo, pentru a sti din timp locatia la care trebuie sa ajunga.
O alta functionalitate a aplicatiei va fi posibilitatea soferului de a salva locatia in care si-a parcat masina, 
de a o modifica si de a adauga note cu privire la aceasta. 
Totodata, utilizatorul va primi o notificare in momentul in care timpul de stationare depaseste durata predefinita 
de acesta la momentul sosirii si calculeaza suma datorata.


